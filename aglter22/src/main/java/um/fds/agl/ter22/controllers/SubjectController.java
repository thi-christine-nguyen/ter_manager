package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.StudentForm;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.forms.TeacherForm;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeacherService;

import java.util.ArrayList;
import java.util.Optional;

import static org.springframework.util.StringUtils.capitalize;

@Controller
public class SubjectController implements ErrorController {

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/listSubjects")
    public Iterable<Subject> getSubjects(Model model) {
        Iterable<Subject> subjects=subjectService.getSubjects();
        model.addAttribute("subjects", subjects);

        return subjects;
    }


    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")//acces à la page
    @GetMapping(value = { "/addSubject" })
    public String showAddSubjectPage(Model model) {

        SubjectForm subjectForm = new SubjectForm();
        model.addAttribute("subjectForm", subjectForm);
        Iterable<Teacher> teachers=teacherService.getTeachers();
        model.addAttribute("teachers", teachers);


        return "addSubject";
    }


    @PostMapping(value = { "/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        Subject subject;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(subjectService.findById(subjectForm.getId()).isPresent()){//update

            subject = subjectService.findById(subjectForm.getId()).get();


            if(auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_MANAGER"))){
                if (subjectForm.getSecondaryTeachers() != null){

                    ArrayList<Teacher> secondaryTeachers = new ArrayList<>();
                    String[] listSecondary = subjectForm.getSecondaryTeachers().split(",");
                    for (int i = 0; i < listSecondary.length; i++){
                        secondaryTeachers.add(teacherService.findByName(capitalize(listSecondary[i])));
                    }

                    subject.setSecondaryTeachers(secondaryTeachers);

                }
                subject.setTitleSubject(subjectForm.getTitleSubject());
                subject.setTeacherSubject(subjectForm.getTeacherSubject());


            }else if (auth.getName().equals(subject.getTeacherSubject().getLastName())){//si teacher avec bon nom
                subject.setTitleSubject(subjectForm.getTitleSubject());
                subject.setTeacherSubject(teacherService.findByName(auth.getName()));

            }


        } else {
            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_MANAGER"))){

                ArrayList<Teacher> secondaryTeachers = new ArrayList<>();
                String[] listSecondary = subjectForm.getSecondaryTeachers().split(",");
                for (int i = 0; i < listSecondary.length; i ++){
                    secondaryTeachers.add(teacherService.findByName(listSecondary[i]));
                }
                subject = new Subject(subjectForm.getTitleSubject(), subjectForm.getTeacherSubject(), secondaryTeachers);

            }else {

                subject = new Subject(subjectForm.getTitleSubject(), teacherService.findByName(auth.getName()), new ArrayList<>());
            }

        }
        subjectService.saveSubject(subject);
        return "redirect:/listSubjects";

    }
    //
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value = {"/showSubjectUpdateForm/{id}"})
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id){

        SubjectForm subjectForm = new SubjectForm(id, subjectService.findById(id).get().getTitleSubject(), subjectService.findById(id).get().getTeacherSubject());
        model.addAttribute("subjectForm", subjectForm);
        Iterable<Teacher> teachers=teacherService.getTeachers();
        model.addAttribute("teachers", teachers);


        return "updateSubject";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id){
        subjectService.deleteSubject(id);

        return "redirect:/listSubjects";
    }
}
