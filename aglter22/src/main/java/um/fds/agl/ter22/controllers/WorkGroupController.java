package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.WorkGroup;

import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.forms.WorkGroupForm;
import um.fds.agl.ter22.services.StudentService;
import um.fds.agl.ter22.services.TeacherService;
import um.fds.agl.ter22.services.WorkGroupService;

import java.util.ArrayList;
import java.util.Optional;

import static org.springframework.util.StringUtils.capitalize;

@Controller
public class WorkGroupController  implements ErrorController {

    @Autowired
    private WorkGroupService workGroupService;
    @Autowired
    private StudentService studentService;

    @GetMapping("/listWorkGroup")
    public Iterable<WorkGroup> getWorkGroup(Model model) {
        Iterable<WorkGroup> workGroups = workGroupService.getWorkGroup();
        model.addAttribute("workGroups", workGroups);

        return workGroups;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")//acces à la page
    @GetMapping(value = {"/addWorkGroup"})
    public String showAddWorkGroupPage(Model model) {

        WorkGroupForm workGroupForm = new WorkGroupForm();
        model.addAttribute("workGroupForm", workGroupForm);


        return "addWorkGroup";
    }

    @PostMapping(value = {"/addWorkGroup"})
    public String addWorkGroup(Model model, @ModelAttribute("WorkGroupForm") WorkGroupForm workGroupForm) {
        WorkGroup workGroup;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (workGroupService.findById(workGroupForm.getId()).isPresent()) {//update

            workGroup = workGroupService.findById(workGroupForm.getId()).get();
            workGroup.setNomGroup(workGroupForm.getNomGroup());

            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_MANAGER"))) {
                if (workGroupForm.getStudents() != null) {

                    ArrayList<Student> students = new ArrayList<>();
                    String[] listStudents = workGroupForm.getStudents().split(",");
                    for (int i = 0; i < listStudents.length; i++) {
                        students.add(studentService.findByName(listStudents[i]));
                    }

                    workGroup.setStudents(students);

                }


            }


        } else {
            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_MANAGER"))) {

                ArrayList<Student> students = new ArrayList<>();
                String[] listStudents = workGroupForm.getStudents().split(",");
                for (int i = 0; i < listStudents.length; i++) {
                    students.add(studentService.findByName(listStudents[i]));
                }
                workGroup = new WorkGroup(workGroupForm.getNomGroup(), students);

            } else {
                //Student crée un nouveau groupe avec lui même dans le groupe
                ArrayList<Student> listStudent = new ArrayList<>();
                listStudent.add(studentService.findByName(auth.getName()));
                workGroup = new WorkGroup(workGroupForm.getNomGroup(), listStudent);
            }

            //}


        }
        workGroupService.saveWorkGroup(workGroup);
        return "redirect:/listWorkGroup";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = {"/showWorkGroupUpdateForm/{id}"})
    public String showWorkGroupUpdateForm(Model model, @PathVariable(value = "id") long id) {

        WorkGroupForm workGroupForm = new WorkGroupForm(id, workGroupService.findById(id).get().getNomGroup());
        model.addAttribute("workGroupForm", workGroupForm);



        return "updateWorkGroup";
    }


    //ne se met pas à jour sur l'affichage
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/deleteWorkGroup/{id}"})
    public String deleteWorkGroup(Model model, @PathVariable(value = "id") long id){

        workGroupService.deleteWorkGroup(id);

        return "redirect:/listWorkGroup";
    }

    @GetMapping(value = {"/registration/{id}"})
    public String registration(Model model, @PathVariable(value = "id") long id){

        Optional<WorkGroup> workGroup = workGroupService.findById(id);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        workGroup.get().addStudent(studentService.findByName(auth.getName()));
        workGroupService.saveWorkGroup(workGroup.get());


        return "redirect:/listWorkGroup";
    }
    @GetMapping(value = {"/unsubscription/{id}"})
    public String unsubscription(Model model, @PathVariable(value = "id") long id){

        Optional<WorkGroup> workGroup = workGroupService.findById(id);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        workGroup.get().deleteStudent(studentService.findByName(auth.getName()));
        workGroupService.saveWorkGroup(workGroup.get());


        return "redirect:/listWorkGroup";
    }
}





