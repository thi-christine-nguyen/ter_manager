package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class WorkGroup {

    @Id @GeneratedValue
    private long id;

    private String nomGroup;

    @OneToMany
    private List<Student> students;


    public WorkGroup(long id, String nomGroup, ArrayList<Student> students) {
        this.id = id;
        this.nomGroup = nomGroup;
        this.students = students;
    }

    public WorkGroup(String nomGroup, ArrayList<Student> students) {
        this.nomGroup = nomGroup;
        this.students = students;
    }

    public WorkGroup() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomGroup() {
        return nomGroup;
    }

    public void setNomGroup(String nomGroup) {
        this.nomGroup = nomGroup;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public void addStudent(Student s){
        this.students.add(s);
    }

    public void deleteStudent(Student student) {
        this.students.remove(student);
    }

    @Override
    public String toString() {
        return "WorkGroup{" +
                "id=" + id +
                ", nomGroup='" + nomGroup + '\'' +
                ", students=" + students +
                '}';
    }
}
