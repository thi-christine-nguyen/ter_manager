package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.WorkGroup;

public interface WorkGroupRepository <T extends WorkGroup> extends CrudRepository<T, Long> {

    //public T findByTitleSubject(String titleSubject);



    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    WorkGroup save(@Param("workGroup") WorkGroup workGroup);


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);



    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("workGroup") WorkGroup workGroup);

}
