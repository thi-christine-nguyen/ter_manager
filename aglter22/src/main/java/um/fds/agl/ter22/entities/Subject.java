package um.fds.agl.ter22.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity

public class Subject {

    private @Id @GeneratedValue Long id;
    private String  titleSubject;
    private @ManyToOne Teacher teacherSubject;

    private @OneToMany List<Teacher> secondaryTeachers = new ArrayList<>();

    public Subject(long id, String titleSubject, Teacher teacherSubject) {
        this.id = id;
        this.titleSubject = titleSubject;
        this.teacherSubject = teacherSubject;
        this.secondaryTeachers = new ArrayList<>();
    }



    public Subject(String titleSubject, Teacher teacherSubject, List<Teacher> secondaryTeachers) {
        this.titleSubject = titleSubject;
        this.teacherSubject = teacherSubject;
        this.secondaryTeachers = secondaryTeachers;
    }

    public Subject() {
    }



    public Long getId() {
        return id;
    }

    public String getTitleSubject() {
        return titleSubject;
    }

    public Teacher getTeacherSubject() {
        return teacherSubject;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitleSubject(String titleSubject) {
        this.titleSubject = titleSubject;
    }

    public void setTeacherSubject(Teacher teacherSubject) {
        this.teacherSubject = teacherSubject;
    }


    public List<Teacher> getSecondaryTeachers() {
        return secondaryTeachers;
    }

    public void setSecondaryTeachers(List<Teacher> secondaryTeachers) {
        this.secondaryTeachers = secondaryTeachers;
    }


    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", titleSubject='" + titleSubject + '\'' +
                ", teacherSubject=" + teacherSubject +
                ", secondaryTeachers=" + secondaryTeachers +
                '}';
    }
}
